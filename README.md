# Narvaez Ruiz Alexis

## Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?

```plantuml
@startmindmap
* Programando ordenadores \nen los 80 y ahora.
** Ahora

*** Nuevas plataformas
**** Recurso de sobra

*** Problemas
**** Muchas llamadas
***** Perdida de tiempo
***** Poco eficiencia
***** Muchos Bugs
**** No funciona\nigual todos \nlos dias
**** Software utliza\nmas recurso

** Antes
*** Descontinuado
*** Sacado del\nmercado
*** Computadoras \nde los 80-90
**** Muy distintas\nentre si
**** Lenguaje maquina
**** Ensamblador
***** Funciona sin\n cosas extras
***** Programas \nmejor planteados
**** Pocos BUGS
**** Cuello de botella\npor el hardware

@endmindmap
```

## Historia de los algoritmos y lenguajes de programación.

```plantuml
@startmindmap
* Historia de los \nalgoritmos y\nlenguajes \nde programación
** Algoritmo
*** Secuencia ordena \nfinita de pasos.
**** Se clasifican.
***** Polinomiales
****** Tiempo de \nejecucion \nrazonble
***** No polinomiales
****** Tiempo de \nejecucion NO\nrazonble
******* Problema del viajero
*** Antigua\nMesopotamia
**** Transacciones comerciales
***** En siglos\nposteriores
****** Siglo XVII
******* Primeras\nCalculadoras\nde bolsillo
****** Siglo XX
******* Maquinas\nProgramables

** Lenguajes de\nProgramación
*** Representacion\nde un algoritmo.
*** Cartones\nPerforados.
**** Joseph Marie\nJacquard
**** Siglo XX
***** Primera\nComputadora
****** Charles Babbage
******* Uso de memoria\npara almacenar\ncalculos
******* John Von\nNeumann
******** Arquitectura\nVonNeumann

***** Mas tarde
****** Aparecen
******* Lenguajes de \nProgramacion
******** Fortan
********* Primer Lenguajes de \nProgramacion\nImperativo
********* Otros\nLenguajes

********** Lisp\n(1958)
*********** Lenguaje\nFuncional

********** Prolog\n(1972)
*********** Lenguaje\nLogico

********** C\n(1969)
*********** Lenguaje\nImperativo

******** Lenguajes\nOrientados\nObjetos
********* Java\n(1996)
********* C++\n(1979)
********* Python\n(1979)



@endmindmap

```

## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005).

```plantuml
@startmindmap

* Evolucion de los \nlenguajes y paradigamas \nde programacion.
** Paradigma
*** Forma de pensar
*** Lenguaje ensmablador.
**** Aparicion de \n otros paradigmas.

***** Programacion \n estructurada.
****** Programacion por \n funciones o rutinas
******* Estructuras de \ncontrol
******** Ejemplos que \n lo aplican

********* ALGOL
********* Pascal
********* Ada

***** Programacion \n logica.

****** Predicado logico
****** Verdadero o Falso
******* Ejemplos que \n lo aplican
******** Prolog
******** Lisp
******** Erlang

***** Programacion \n funcional.
***** Lenguaje de \nlas matematicas
****** Recursividad 
******* Ejemplos que \n lo aplican 
******** Haskell
******** OCaml 
******** F#


***** Programacion \n Orientada a Objetos.

****** Abstraccion del \nmundo real
****** Entrada y salidas
******* Ejemplos que \n lo aplican
******** Java
******** JavaScript
******** PHP


***** Programacion \n distbuida.

****** Computadoras \nconectadas entre si
****** Grandes distancias
******* Ejemplos que \n lo aplican
******** Ada
******** Alef
******** Erlang


***** Programacion \n Orientada a aspectos.

****** Modulacion \nde Apps
******* Separar \nresponsibilidades
******** Ejemplo
********* Cool
********* D
***** Programacion Orientada a\n agentes de software.

****** Apps de \nrecomendacion
******* Basada \nen agentes
******* Sistemas \nmultiagentes
******* Ejemplos que \n lo aplican
******** JavaLog



@endmindmap
```
